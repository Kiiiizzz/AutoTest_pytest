import os
import sys


class Logger(object):
    def __init__(self, fileN="Default.log"):
        self.terminal = sys.stdout
        self.log = open(fileN, "w")

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)
        self.flush()

    def flush(self):
        self.log.flush()


sys.stdout = Logger("target_file.txt")
os.system('pytest -v --alluredir ./report/result --clean-alluredir')
