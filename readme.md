config: 配置文件夹
    -- config: 全局配置
    -- config_cases: 用例中相关的配置数据
    
report: 生成的报告
    -- allure-report: allure生成的测试报告
    -- result: allure生成的测试数据
    
testcase: 测试用例

util: 工具类

requirments.txt: 第三方包

runAll: 运行全部用例

readme: 项目说明书


生成requirements.txt命令:
    pip freeze > requirements.txt