import os
import json
from datetime import datetime

root_path = os.path.abspath(os.path.dirname(__file__)).split('AutoTest_pytest')[0]
yamlpath = str(root_path) + "AutoTest_pytest\\config\\config.yaml"
print(root_path)
print(os.path.dirname(__file__).split('AutoTest_pytest')[0])
print(str(__file__).split('AutoTest_pytest')[0])

a = '{"errno":0,"errmsg":null,"unassigned":0,"total":0,"list":null}'
b = json.loads(a)
print(type(a), json.loads(a), type(b))
c = json.dumps(b)
print(type(c), c)

print(datetime.now().strftime("%Y%m%d_%H%M%S"))