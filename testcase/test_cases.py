# -*- coding: utf-8 -*-
# @Time : 2021/1/11 20:46
# @Author : hwk
# @FileName: test_cases.py
# @Software: PyCharm
import base64
import json
from hashlib import md5

import pytest

from util.db_util import MysqlDb
from util.request_util import RequestUtil
from util.encrypt import encrypt_zhongcai, encrypt_kaidan


class TestCase:

    @pytest.mark.xfail()
    @pytest.mark.parametrize("case", MysqlDb().query('select * from `case_zhongcailiuyan`', state='all'))
    def test_zhongcailiuyan(self, case):
        data = encrypt_zhongcai(case)
        response = RequestUtil().request(url=case['url'], method=case['method'], headers=json.loads(case['headers']),
                                         param=data)
        # MysqlDb().updateResultByCaseId(case, response)
        assert response == case['expect_result']

    #
    # @pytest.mark.parametrize("case", MysqlDb().query('select * from `case_login`', state='all'))
    # def test_login(self, case):
    #     response = RequestUtil().request(url=case['url'], method=case['method'], headers=json.loads(case['headers']),
    #                                      param=json.loads(case['request_body']))
    #     MysqlDb().updateResultByCaseId(case, response)
    #     assert response['code'] == int(case['expect_result'])

    # @pytest.mark.parametrize("case", MysqlDb().query('select * from `case_kaidan`', state='all'))
    # def test_kaidan(self, case):
    #     data = encrypt_kaidan(case)
    #     print(case['request_body'])
    #     response = RequestUtil().request(url=case['url'], method=case['method'], headers=json.loads(case['headers']),
    #                                      param=data)
    #     MysqlDb().updateResultByCaseId(case, response)
    #     assert response == case['expect_result']

    @pytest.mark.parametrize("case", MysqlDb().query('select * from `case_wangdiandenglu`', state='all'))
    def test_wangdiandenglu(self, case):
        response = RequestUtil().request(url=case['url'], method=case['method'], headers=json.loads(case['headers']),
                                         param=case['request_body'])

        # MysqlDb().updateResultByCaseId(case, response)
        assert response == json.loads(case['expect_result'])

    @pytest.mark.parametrize("case", MysqlDb().query('select * from `case_wangdiankaidan`', state='all'))
    def test_wangdiankaidan(self, case):
        response = RequestUtil().request(url=case['url'], method=case['method'], headers=json.loads(case['headers']),
                                         param=case['request_body'])

        # MysqlDb().updateResultByCaseId(case, response)
        assert response == json.loads(case['expect_result'])


if __name__ == '__main__':
    pytest.main()
    # pytest -v -m "not aaa"
    # pytest.main([r'--alluredir=target/allure-results', '–-clean-alluredir'])
    # res = os.popen('pytest --alluredir report/results')
    # str = res.read()
    # print(str)
    # os.system('allure serve .target/allure-results')
    # '–-clean-alluredir'
    # os.system('allure generate ./target/allure-results -o allure-report --clean')
    # os.system('allure serve allure-report')
    # os.system('pytest --alluredir report/xml')
    # os.system('allure generate report/xml -o report/html')
    #
    # pytest --alluredir target/allure-results
    # allure serve target/allure-results
