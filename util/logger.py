# -*- coding: utf-8 -*-
# @FileName: logger.py
# @Author: hwk
# @Time: 2021/6/7 17:13

import os
import logging
from datetime import datetime


class Logger:
    def __init__(self):
        self.logger = logging.getLogger()
        if not self.logger.handlers:
            self.logger.setLevel(logging.DEBUG)

            # 创建一个handler，用于写入日志文件
            fh = logging.FileHandler(self.log_path, encoding='utf-8')
            fh.setLevel(logging.DEBUG)

            # 在控制台输出
            ch = logging.StreamHandler()
            ch.setLevel(logging.INFO)

            # 定义hanler的格式
            formatter = logging.Formatter(self.fmt)
            fh.setFormatter(formatter)
            ch.setFormatter(formatter)

            # 给log添加handles
            self.logger.addHandler(fh)
            self.logger.addHandler(ch)

    @property
    def fmt(self):
        return '%(asctime)s %(levelname)s %(filename)s:%(lineno)d %(message)s'

    @property
    def log_path(self):
        root_path = os.path.abspath(os.path.dirname(__file__)).split('AutoTest_pytest')[0]
        LOG_PATH = str(root_path) + "AutoTest_pytest\\log"
        if not os.path.exists(LOG_PATH):
            os.makedirs(LOG_PATH)
        return os.path.join(LOG_PATH, '{}.log'.format(datetime.now().strftime("%Y%m%d_%H%M%S")))


log = Logger().logger
if __name__ == '__main__':
    log.info("你好")