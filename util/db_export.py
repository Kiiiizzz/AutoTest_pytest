# -*- coding: utf-8 -*-
# @Time : 2020/11/28 14:59
# @Author : hwk
# @FileName: db_export.py
# @Software: PyCharm

"""
导出数据库到xls

"""

import importlib
import sys
import os
import pymysql
import xlwt
import time

importlib.reload(sys)


class db_export_to_excel:

    def export(host, user, password, dbname, table_name, outputpath):
        conn = pymysql.connect(host, user, password, dbname, charset='utf8')
        cursor = conn.cursor()

        count = cursor.execute('select * from ' + '`' + table_name + '`')
        # 重置游标的位置
        cursor.scroll(0, mode='absolute')
        # 搜取所有结果
        results = cursor.fetchall()

        # 获取MYSQL里面的数据字段名称`
        fields = cursor.description
        workbook = xlwt.Workbook()
        sheet = workbook.add_sheet('table_' + table_name, cell_overwrite_ok=True)

        # 写上字段信息
        for field in range(0, len(fields)):
            sheet.write(0, field, fields[field][0])

        # 获取并写入数据段信息
        row = 1
        col = 0
        for row in range(1, len(results) + 1):
            # print(row)
            # print(col)
            for col in range(0, len(fields)):
                sheet.write(row, col, u'%s' % results[row - 1][col])

        workbook.save(outputpath)


# 结果测试
if __name__ == "__main__":
    outputpath = os.path.join(os.path.abspath(os.path.join(os.getcwd(), "..")), r'report')
    print(outputpath)
    now = time.strftime("%Y-%m-%d_%H_%M_%S", time.localtime(time.time()))
    db_export_to_excel.export('127.0.0.1', 'root', '499681038a', 'api_test', 'case', outputpath + '\降龙十八掌' + now)
