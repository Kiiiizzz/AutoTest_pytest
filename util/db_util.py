import datetime
import os
from warnings import filterwarnings

import pymysql
import yaml

# 忽略Mysql告警信息

filterwarnings("ignore", category=pymysql.Warning)


class MysqlDb:

    def __init__(self):
        root_path = os.path.abspath(os.path.dirname(__file__)).split('AutoTest_pytest')[0]
        yamlpath = str(root_path) + "AutoTest_pytest\\config\\config.yaml"
        # yamlpath = os.path.abspath(os.path.join(os.getcwd(), "./config/config.yaml"))
        f = open(yamlpath, 'r', encoding='utf-8')
        # 读取yaml文件
        content = yaml.load(f.read(), Loader=yaml.FullLoader)
        # 建立数据库连接
        self.conn = pymysql.connect(host=content['db_mysql']['ip'], user=content['db_mysql']['user'],
                                    password=str(content['db_mysql']['pwd']),
                                    database=content['db_mysql']['db'])

        # 使用 cursor 方法获取操作游标，得到一个可以执行sql语句，并且操作结果作为字典返回的游标
        self.cur = self.conn.cursor(cursor=pymysql.cursors.DictCursor)

    def __del__(self):
        # 关闭游标
        self.cur.close()
        # 关闭连接
        self.conn.close()

    def query(self, sql, state="one"):
        """
        查询
        :param sql:
        :param state: all是默认查询全部
        :return:
        """
        self.cur.execute(sql)

        if state == "all":
            # 查询全部
            data = self.cur.fetchall()
        else:
            # 查询单条
            data = self.cur.fetchone()
        return data

    def execute(self, sql):
        """
        更新、删除、新增
        :param sql:
        :return:
        """
        try:
            # 使用execute操作sql
            rows = self.cur.execute(sql)
            # 提交事务
            self.conn.commit()
            return rows
        except Exception as e:
            print("数据库操作异常 {0}".format(e))
            # 回滚修改
            self.conn.rollback()

    def updateResultByCaseId(self, case, response):
        """
        根据测试用例id，更新响应内容和测试内容
        :param case:
        :param response:
        :return:
        """
        # print("***--------------updateResultByCaseId-------------***")
        my_db = MysqlDb()
        current_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        sql = "update `{0}` set response=\"{1}\", update_time='{2}' where id={3}".format(case['target'],
                                                                                         str(response), current_time,
                                                                                         case['id'])
        rows = my_db.execute(sql)
        return rows


if __name__ == '__main__':
    mydb = MysqlDb()
    r = mydb.query("select * from `case`")
    # r = mydb.execute("insert into `case` (`app`) values('hwk')")
    print(r)
