# -*- coding: utf-8 -*-
# @FileName: encrypt.py
# @Author: hwk
# @Time: 2021/4/30 16:47
import base64
from _md5 import md5

from util.yaml_util import yalmRead_cases


def encrypt_zhongcai(case):
    """
    仲裁类加密方法
    :param case:
    :return:
    """

    data_64 = base64.b64encode(case['request_body'].encode())
    validation_demo = str(data_64) + yalmRead_cases()['zhongcailiuyan']['partnerid'] + str(yalmRead_cases()['zhongcailiuyan']['secret_key'])
    data_split = (str(data_64)).split("'")
    data_s1 = data_split[1]
    validation_split = validation_demo.split("'")
    validation_md5 = validation_split[1] + validation_split[2]
    validation = md5(validation_md5.encode('utf8')).hexdigest()
    data = 'method=' + case['api_name'] + '&partnerid=' + yalmRead_cases()['zhongcailiuyan'][
        'partnerid'] + '&data=' + data_s1 + '&validation=' + validation
    print(data_s1, validation)
    return data

def encrypt_kaidan(case):
    """
    :param case:
    开单类加密方法 b
    :return:
    """

    data_64 = base64.b64encode(str(case['request_body']).encode())
    validation_demo = str(data_64) + str(yalmRead_cases()['kaidan']['partnerid']) + str(yalmRead_cases()['kaidan']['secret_key'])
    data_split = (str(data_64)).split("'")
    data_s1 = data_split[1]
    validation_split = validation_demo.split("'")
    validation_md5 = validation_split[1] + validation_split[2]
    validation = md5(validation_md5.encode('utf8')).hexdigest()
    data = 'method=' + case['api_name'] + '&partnerid=' + str(yalmRead_cases()['kaidan'][
        'partnerid']) + '&version=' + str(yalmRead_cases()['kaidan'][
        'version']) + '&data=' + data_s1 + '&validation=' + validation + '&systemid=' + str(yalmRead_cases()['kaidan'][
        'systemid'])
    return data