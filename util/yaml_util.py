# -*- coding: utf-8 -*-
# @FileName: yaml_util.py
# @Author: hwk
# @Time: 2021/4/30 14:28
import os

import yaml


def yalmRead_config():
    # 获取项目根目录
    root_path = os.path.abspath(os.path.dirname(__file__)).split('AutoTest_pytest')[0]
    yamlpath = str(root_path)+"AutoTest_pytest\\config\\config.yaml"
    f = open(yamlpath, 'r', encoding='utf-8')
    # 读取yaml文件
    content = yaml.load(f.read(), Loader=yaml.FullLoader)
    return content

def yalmRead_cases():
    # 获取项目根目录
    root_path = os.path.abspath(os.path.dirname(__file__)).split('AutoTest_pytest')[0]
    yamlpath = str(root_path)+"AutoTest_pytest\\config\\config_cases.yaml"
    f = open(yamlpath, 'r', encoding='utf-8')
    # 读取yaml文件
    content = yaml.load(f.read(), Loader=yaml.FullLoader)
    return content