import requests

"""
Http工具类封装
"""


class RequestUtil:

    def __init__(self):
        pass

    # def runcase_by_tablename(self, tablename):
    #
    #     mysql = MysqlDb()
    #     sql = 'select * from ' + '`' + tablename + '`'
    #     case = mysql.query(sql)
    #     headers = json.loads(case['headers'])
    #     body = json.loads(case['request_body'])
    #     method = case['method']
    #     req_url = configFile.HOST + case['url']
    #     response = RequestUtil().request(req_url, method, headers=headers, param=body)
    #     return response

    def request(self, url, method, headers=None, param=None, content_type=None):
        """
        通用请求工具类
        :param url:
        :param method:
        :param headers:
        :param param:
        :param content_type:
        :return:
        """
        try:
            if method == 'get':
                result = requests.get(url=url, params=param, headers=headers).json()
                return result
            elif method == 'post':
                if content_type == 'application/json':
                    result = requests.post(url=url, json=param, headers=headers).json()
                    return result
                else:
                    result = requests.post(url=url, data=param, headers=headers).json()
                    return result
            else:
                print("http method not allowed")

        except Exception as e:
            print("http请求报错:{0}".format(e))


if __name__ == '__main__':
    pass
